import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import Manager from '../../state/manager.js';

function PageSlider(props) {
    const navigate = useNavigate();
    const [username, setUsername] = useState('');

    const manager = new Manager();
    
    const handleClick = () => {
        manager.updateState('username', '');
        manager.updateState('passwd', '');
        navigate('/');
    };

    useEffect(() => {
        setUsername(manager.getState('username'));
    }, []);
    
    return (
        <>
            <span>{username}</span>
            <button onClick={handleClick}>Выйти</button>
        </>
    );
}

export default PageSlider;