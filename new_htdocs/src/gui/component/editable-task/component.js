import { useState, useEffect } from 'react';
import Manager from '../../state/manager.js';

function EditableTask(props) {
    const [value1, setValue1] = useState(0);
    const [value2, setValue2] = useState(0);

    const manager = new Manager();

    const handleValue = (number, event) => {
        const value = Number(event.target.value);
        if (isNaN(value)) return 0;

        if (number == 1) setValue1(value);
        if (number == 2) setValue2(value);
    };

    const handleEnter = (event) => {
        if (event.key != 'Enter') return;

        manager.updateState('value1', value1);
        manager.updateState('value2', value2);
        manager.query('create');
    };

    const tds = ['ID', 'value1', 'value2', 'result', 'status', 'mgmt'].map(option => {
        if (option == 'value1') {
            return (
                <td style={{
                    border: '1px solid grey'
                }} key={option}>
                    <input onChange={(event) => handleValue(1, event)} type='text' value={value1} />
                </td>
            );
        }
        if (option == 'value2') {
            return (
                <td style={{
                    border: '1px solid grey'
                }} key={option}>
                    <input onChange={(event) => handleValue(2, event)} type='text' value={value2} />
                </td>
            );
        }
        return (
            <td style={{
                border: '1px solid grey'
            }} key={option}/>
        );
    });

    return (
        <tr onKeyUp={handleEnter}>
        {tds}
        </tr>
    )
}

export default EditableTask;