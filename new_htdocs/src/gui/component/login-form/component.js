import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import Manager from '../../state/manager.js';

function LoginForm(props) {
    const [username, setUsername] = useState('');
    const [passwd, setPasswd] = useState('');
    const navigate = useNavigate();

    const manager = new Manager();

    const handleUsername = (event) => {
        manager.updateState('username', event.target.value);
    };
    const handlePasswd = (event) => {
        manager.updateState('passwd', event.target.value);
    };
    const handleLogin = () => {
        if (!username) {
            manager.updateState('error', 'Введите логин!');
            return;
        }
        if (!passwd) {
            manager.updateState('error', 'Введите пароль!');
            return;
        }
        manager.query('login');
    };

    const checkState = (stateName, state) => {
        switch(stateName) {
            case 'login':
                if (state.status === 'OK' && state.token !== 'BAD') {
                    setUsername(username);
                    navigate('/tasks');
                }
                else manager.updateState('error', 'Неправильный логин или пароль!');
                break;
            case 'username':
                setUsername(state);
                break;
            case 'passwd':
                setPasswd(state);
                break;
        }
    };
    const unsubscribe = () => {
        for (let i = 0; i < 3; i++)
            manager.unsubscribe(checkState);
    };

    useEffect(() => {
        const subscribe = async () => {
            await manager.subscribe('login', checkState);
            await manager.subscribe('username', checkState);
            await manager.subscribe('passwd', checkState);
        };
        subscribe();
        return unsubscribe;
    }, []);

    return (
        <>
            <input type="text" placeholder="Имя пользователя" value={username} onChange={handleUsername} />
            <input type="password" placeholder="Пароль" value={passwd} onChange={handlePasswd} />
            <button onClick={handleLogin}>Войти</button>
        </>
    );
}

export default LoginForm;