import { useState, useEffect } from 'react';
import Manager from '../../state/manager.js';
import TaskHeader from '../task-header/component.js';
import Task from '../task/component.js';
import EditableTask from '../editable-task/component.js';

function TaskTable(props) {
    const [tasks, setTasks] = useState([]);
    
    const manager = new Manager();

    const handleUpdate = () => {
        manager.query('tasks');
    };
    const handleAdd = () => {
        setTasks(manager.getState('tasks').concat({editable: true}));
    };

    const checkState = (stateName, state) => {
        switch (stateName) {
            case 'tasks':
                setTasks(state.list);
                break;
        }
    };
    const unsubscribe = () => {
        manager.unsubscribe(checkState);
    };

    useEffect(() => {
        const subscribe = async () => {
            await manager.subscribe('tasks', checkState, true);
            await manager.subscribe('calculation', checkState);
        };
        subscribe();
        return unsubscribe;
    }, []);

    const trs = tasks.sort((a, b) => a.ID - b.ID).map(task => {
        if (task.editable) return (<EditableTask key='1337' />);
        return (<Task key={task.ID} ID={task.ID} value1={task.value1} value2={task.value2} result={task.result} status={task.status} />);
    });

    return (
        <>
            <button onClick={handleUpdate}>Обновить</button>
            <button onClick={handleAdd}>Добавить</button>
            <table style={{
                border: '2px solid grey',
                textAlign: 'center',
                backgroundColor: 'lightgray'
            }}>
                <tbody>
                    <TaskHeader />
                    {trs}
                </tbody>
            </table>
        </>
    );
}

export default TaskTable;