import { useState, useEffect } from 'react';
import Manager from '../../state/manager.js';

function ErrorWindow(props) {
    const [message, setMessage] = useState('');

    const manager = new Manager();

    const checkState = (stateName, state) => {
        switch (stateName) {
            case 'error':
                setMessage(state);
                setTimeout(() => {
                    setMessage('');
                }, 2000);
                break;
        }
    };
    const unsubscribe = () => {
        manager.unsubscribe(checkState);
    };
 
    useEffect(() => {
        const subscribe = async () => {
            manager.subscribe('error', checkState);
        };
        subscribe();
        return unsubscribe;
    }, []);

    return (
        <form> 
            <span>{message}</span>
        </form>
    );
}

export default ErrorWindow;