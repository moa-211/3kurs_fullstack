import PageSlider from '../../component/page-slider/component.js';
import TaskTable from '../../component/task-table/component.js';
import TaskStatus from '../../component/task-status/component.js';
import ErrorWindow from '../../component/error-window/component.js';

function TaskPage(props) {
    return (
        <>
            <TaskTable />
            <PageSlider />
            <TaskStatus />
            <ErrorWindow />
        </>
    );
}

export default TaskPage;