import LoginForm from '../../component/login-form/component.js';
import ErrorWindow from '../../component/error-window/component.js';

function LoginPage(props) {
    return (
        <>
            <LoginForm />
            <ErrorWindow />
        </>
    );
}

export default LoginPage;