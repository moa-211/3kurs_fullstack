export async function login({username, passwd})
{
    const response = await fetch(`http://localhost:8080/fullstack-1.0/api/v1/auth?username=${username}&passwd=${passwd}`);
    const content = await response.json();

    return content.message;
}

export async function getTasks({token})
{
    let response = await fetch(`http://localhost:8080/fullstack-1.0/api/v1/tasks`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });
    const content = await response.json();

    return JSON.parse(content.message);
}

export async function createTask({token, value1, value2})
{
    let response = await fetch(`http://localhost:8080/fullstack-1.0/api/v1/tasks`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
            value1: value1,
            value2: value2
        })
    });
    const content = await response.json();

    return content.message === 'OK';
}

export async function deleteTask({token, id})
{
    let response = await fetch(`http://localhost:8080/fullstack-1.0/api/v1/tasks?id=${id}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });
    const content = await response.json();

    return content.message;
}

export async function startCalculation({token, id})
{
    let response = await fetch(`http://localhost:8080/fullstack-1.0/api/v1/calculations`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
            ID: id
        })
    });
    const content = await response.json();

    return content.message;
}