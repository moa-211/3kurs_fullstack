import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import LoginPage from './gui/page/login-page/component.js';
import TaskPage from './gui/page/task-page/component.js';

const pgLogin = (<LoginPage />);
const pgTask = (<TaskPage />);

function App() {  
  const router = (
  <Router>
      <Routes>
          <Route path='' element={pgLogin} />
          <Route path='/tasks' element={pgTask} />
      </Routes>
  </Router>
  );
  return router; 
}

export default App;