package local.app;

import local.app.dto.TaskDTO;

import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import local.infrastructure.data.IFileExplorer;
import local.infrastructure.utils.ITokenManager;
import local.core.CalculatorFactory;

public interface IApp {
    public String login(String username, String passwd);

    public int calc(int value1, int value2);

    public List<TaskDTO> getTasks(String username);

    public boolean createTask(String username, int value1, int value2);

    public boolean deleteTask(String username, int ID);

    public boolean startCalculation(String username, int ID);

    public boolean validateToken(String token);
    public Map<String, String> getUserInfo(String token);
}

class App implements IApp, StorageInjection, TokenManagerInjection {
    public IFileExplorer usedExplorer;
    public ITokenManager usedManager;

    @Override
    public String login(String username, String passwd) {
        String token = usedManager.generateToken(username, passwd);
        return validateToken(token) ? token : "BAD";
    }

    @Override
    public int calc(int value1, int value2) {
        return new CalculatorFactory().createInstance().calc(value1, value2);
    }

    @Override
    public List<TaskDTO> getTasks(String username) {
        List<String> lines = usedExplorer.getTasks(username);
        List<TaskDTO> tasks = new ArrayList<>();

        for (String line : lines) {
            TaskDTO dto = new TaskDTO();
            String[] taskElems = line.split(" ");
            for (int i = 0; i < taskElems.length; i++) {
                switch (i) {
                    case 0 -> dto.setID(Integer.parseInt(taskElems[i]));
                    case 1 -> dto.setValue1(Integer.parseInt(taskElems[i]));
                    case 2 -> dto.setValue2(Integer.parseInt(taskElems[i]));
                    case 3 -> {
                        if (!taskElems[i].equals("X")) {
                            dto.setResult(Integer.parseInt(taskElems[i]));
                        }
                    }
                    case 4 -> dto.setStatus(taskElems[i]);
                }
            }
            tasks.add(dto);
        }
        return tasks;
    }

    @Override
    public boolean createTask(String username, int value1, int value2) {
        return usedExplorer.createTask(username, value1, value2);
    }

    @Override
    public boolean deleteTask(String username, int ID) {
        return usedExplorer.modifyTask(username, ID, 0, "DELETE");
    }

    @Override
    public boolean startCalculation(String username, int ID) {
        Map<String, Integer> values = usedExplorer.getTaskValues(username, ID);
        int value1 = values.get("value1");
        int value2 = values.get("value2");
        int result = calc(value1, value2);

        usedExplorer.modifyTask(username, ID, result, "Processing");
        return usedExplorer.modifyTask(username, ID, result, "Processed");
    }

    @Override
    public void injectStorage(IFileExplorer explorer) {
        usedExplorer = explorer;
    }

    @Override
    public void injectTokenManager(ITokenManager manager) {
        usedManager = manager;
    }

    @Override
    public boolean validateToken(String token) {
        Map<String, String> tokenInfo = usedManager.getTokenInfo(token);
        return usedExplorer.findUser(tokenInfo.get("username"), tokenInfo.get("passwd"));
    }

    @Override
    public Map<String, String> getUserInfo(String token) {
        return usedManager.getTokenInfo(token);
    }
}
