package local.app.dto;

public class TaskDTO {
    private int ID;
    private int value1;
    private int value2;
    private int result;
    private String status;

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setValue1(int value1) {
        this.value1 = value1;
    }

    public void setValue2(int value2) {
        this.value2 = value2;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getID() {
        return ID;
    }

    public int getValue1() {
        return value1;
    }

    public int getValue2() {
        return value2;
    }

    public int getResult() {
        return result;
    }

    public String getStatus() {
        return status;
    }
}
