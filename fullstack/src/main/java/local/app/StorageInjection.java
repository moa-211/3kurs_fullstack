package local.app;

import local.infrastructure.data.IFileExplorer;

public interface StorageInjection {
    public void injectStorage(IFileExplorer explorer);
}
