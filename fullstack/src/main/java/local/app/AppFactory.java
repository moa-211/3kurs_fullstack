package local.app;

interface IAppFactory {
    public IApp createInstance();
}

public class AppFactory implements IAppFactory {
    @Override
    public IApp createInstance() {
        return new App();
    }
}