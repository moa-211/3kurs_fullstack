package local.app;

import local.infrastructure.utils.ITokenManager;

public interface TokenManagerInjection {
    public void injectTokenManager(ITokenManager manager);
}
