package local.core;

public interface ICalculator 
{
    public int calc(int value1, int value2);
}
