package local.core;

interface ICalculatorFactory {
    public ICalculator createInstance();
}

public class CalculatorFactory implements ICalculatorFactory {
    @Override
    public ICalculator createInstance() {
        return new Calculator();
    }
}

class Calculator implements ICalculator {
    @Override
    public int calc(int value1, int value2) {
        return value1 + value2;
    }
}
