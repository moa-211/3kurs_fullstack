package local.infrastructure.controller.back;

import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import local.app.IApp;
import local.app.dto.DTO;
import local.app.dto.TaskDTO;
import local.app.dto.ResponseDTO;
import local.infrastructure.builder.Built;

import java.util.List;

import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;

import jakarta.inject.Inject;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

@Path("/tasks")
public class TasksController {
    ResponseDTO response = new ResponseDTO();
    Jsonb jsonb = JsonbBuilder.create();
    String resultJSON;
    @Inject
    @Built
    private IApp app;

    @Context
    private HttpHeaders headers;

    @GET
    @Produces("application/json")
    public Response getTasks() {
        String token = headers.getHeaderString(HttpHeaders.AUTHORIZATION).replace("Bearer ", "");

        if (!app.validateToken(token)) {
            response.setMessage("Ошибка авторизации");
            resultJSON = jsonb.toJson(response);

            return Response.status(Response.Status.UNAUTHORIZED).entity(resultJSON).build();
        }

        String username = app.getUserInfo(token).get("username");
        List<TaskDTO> tasksList = app.getTasks(username);
        response.setMessage(jsonb.toJson(tasksList));
        resultJSON = jsonb.toJson(response);

        return Response.ok(resultJSON).build();
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response createTask(String dto) {
        DTO body = jsonb.fromJson(dto, DTO.class);

        String token = headers.getHeaderString(HttpHeaders.AUTHORIZATION).replace("Bearer ", "");

        if (!app.validateToken(token)) {
            response.setMessage("Ошибка авторизации");
            resultJSON = jsonb.toJson(response);

            return Response.status(Response.Status.UNAUTHORIZED).entity(resultJSON).build();
        }

        String username = app.getUserInfo(token).get("username");
        boolean state = app.createTask(username, body.getValue1(), body.getValue2());
        response.setMessage(state ? "OK" : "BAD");
        resultJSON = jsonb.toJson(response);

        return Response.ok(resultJSON).build();
    }

    @DELETE
    @Produces("application/json")
    public Response deleteTask(
            @QueryParam("id")
            String ID) {
        String token = headers.getHeaderString(HttpHeaders.AUTHORIZATION).replace("Bearer ", "");

        if (!app.validateToken(token)) {
            response.setMessage("Ошибка авторизации");
            resultJSON = jsonb.toJson(response);

            return Response.status(Response.Status.UNAUTHORIZED).entity(resultJSON).build();
        }

        String username = app.getUserInfo(token).get("username");
        boolean state = app.deleteTask(username, Integer.parseInt(ID));
        response.setMessage(state ? "OK" : "BAD");
        resultJSON = jsonb.toJson(response);

        return Response.ok(resultJSON).build();
    }
}
