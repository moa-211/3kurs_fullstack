package local.infrastructure.controller.back;

import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import local.app.IApp;
import local.app.dto.DTO;
import local.app.dto.ResponseDTO;
import local.infrastructure.builder.Built;

import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;

import jakarta.inject.Inject;

import jakarta.ws.rs.POST;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

@Path("/calculations")
public class CalculationsController {
    private ResponseDTO response = new ResponseDTO();
    private Jsonb jsonb = JsonbBuilder.create();
    private String resultJSON;
    @Inject
    @Built
    private IApp app;

    @Context
    private HttpHeaders headers;

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response startCalculation(String dto) {
        DTO body = jsonb.fromJson(dto, DTO.class);

        String token = headers.getHeaderString(HttpHeaders.AUTHORIZATION).replace("Bearer ", "");

        if (!app.validateToken(token)) {
            response.setMessage("Ошибка авторизации");
            resultJSON = jsonb.toJson(response);

            return Response.status(Response.Status.UNAUTHORIZED).entity(resultJSON).build();
        }

        String username = app.getUserInfo(token).get("username");
        boolean state = app.startCalculation(username, body.getID());
        response.setMessage(state ? "OK" : "BAD");
        resultJSON = jsonb.toJson(response);

        return Response.ok(resultJSON).build();
    }
}

