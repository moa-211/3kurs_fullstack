package local.infrastructure.controller.back;

import local.app.IApp;
import local.app.dto.ResponseDTO;
import local.infrastructure.builder.Built;

import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;

import jakarta.inject.Inject;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

@Path("/auth")
public class LoginController {
    ResponseDTO response = new ResponseDTO();
    Jsonb jsonb = JsonbBuilder.create();
    String resultJSON;
    @Inject
    @Built
    private IApp app;

    @GET
    @Produces("application/json")
    public Response login(
            @DefaultValue("")
            @QueryParam("username")
            String username,
            @DefaultValue("")
            @QueryParam("passwd")
            String passwd) {
        if (username.equals("") || passwd.equals("")) {
            response.setMessage("Логин и/или пароль не указаны в качестве параметров");
            resultJSON = jsonb.toJson(response);

            return Response.status(Response.Status.BAD_REQUEST).entity(resultJSON).build();
        }

        String token = app.login(username, passwd);
        response.setMessage(token);
        resultJSON = jsonb.toJson(response);

        return Response.ok(resultJSON).build();
    }
}
