package local.infrastructure.builder;

import local.app.IApp;
import jakarta.enterprise.inject.Default;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import local.app.StorageInjection;
import local.app.TokenManagerInjection;
import local.infrastructure.data.IFileExplorer;
import local.infrastructure.utils.ITokenManager;

interface IBuilder {
    public IApp buildApp();
}

public class Builder implements IBuilder {
    @Inject
    @Default
    private IApp app;

    @Inject
    @Product
    private IFileExplorer explorer;

    @Inject
    @Default
    private ITokenManager manager;

    @Produces
    @Built
    public IApp buildApp() {
        ((StorageInjection) app).injectStorage(explorer);
        ((TokenManagerInjection) app).injectTokenManager(manager);
        return app;
    }
}
