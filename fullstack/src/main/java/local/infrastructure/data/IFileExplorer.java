package local.infrastructure.data;

import java.util.Map;
import java.util.List;

public interface IFileExplorer 
{
    public boolean findUser(String username, String passwd);
    public List<String> getTasks(String username);
    public boolean createTask(String username, int value1, int value2);
    public boolean modifyTask(String username, int ID, int result, String status);
    public Map<String, Integer> getTaskValues(String username, int ID);
    public int getLastID(String username);
}
