package local.infrastructure.data;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;

import jakarta.enterprise.inject.Produces;
import local.infrastructure.builder.Product;

interface IFileExplorerFactory {
    public IFileExplorer createInstance();
}

public class FileExplorerFactory implements IFileExplorerFactory {
    @Override
    @Produces
    public IFileExplorer createInstance() {
        return new FileExplorer();
    }
}

class MockFactory implements IFileExplorerFactory {
    @Override
    @Produces
    public IFileExplorer createInstance() {
        return new Mock();
    }
}

class DBExplorerFactory implements IFileExplorerFactory {
    @Override
    @Produces
    @Product
    public IFileExplorer createInstance() {
        return new DBExplorer();
    }
}


class FileExplorer implements IFileExplorer {
    public static String dataPath = "C:\\3kurs_fullstack\\fullstack\\src\\main\\java\\local\\files";
    public static String usersFile = "users.txt";

    @Override
    public boolean findUser(String username, String passwd) {
        File file = new File(dataPath + "\\" + usersFile);
        String userInfo = username + " " + passwd;
        boolean found = false;

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String st;
            while ((st = br.readLine()) != null) {
                if (st.equals(userInfo)) {
                    found = true;
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return found;
    }

    @Override
    public List<String> getTasks(String username) {
        List<String> lines = new ArrayList<>();
        File file = new File(dataPath + "\\" + username + ".txt");

        if (!file.exists()) return new ArrayList<>();

        try (Scanner sc = new Scanner(file)) {
            while (sc.hasNextLine()) lines.add(sc.nextLine().strip());
        } catch (IOException e) {
            System.out.println(e.toString());
        }

        return lines.size() != 0 ? lines : new ArrayList<>();
    }

    @Override
    public boolean createTask(String username, int value1, int value2) {
        File file = new File(dataPath + "\\" + username + ".txt");

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))) {
            if (!file.exists()) Files.createFile(Paths.get(dataPath + "\\" + username + ".txt"));
            String ID = String.valueOf(getLastID(username));
            String[] taskParams = new String[]{ID, String.valueOf(value1), String.valueOf(value2), "X", "Waiting"};
            writer.write(String.join(" ", taskParams) + '\n');
        } catch (IOException e) {
            System.out.println(e.toString());
            return false;
        }

        return true;
    }

    @Override
    public boolean modifyTask(String username, int ID, int result, String status) {
        try {
            Path path = Paths.get(dataPath + "\\" + username + ".txt");
            List<String> arr = getTasks(username);
            boolean clearFile = false;

            for (int i = 0; i < arr.size(); i++) {
                int fileID = Integer.parseInt(arr.get(i).split(" ")[0]);

                if (fileID == ID && status.equals("Processing")) {
                    String new_value = arr.get(i).replace("Waiting", status);
                    arr.set(i, new_value);
                } else if (fileID == ID && status.equals("Processed")) {
                    String new_value = arr.get(i).replace("X", String.valueOf(result)).replace("Processing", status);
                    arr.set(i, new_value);
                } else if (fileID == ID && status.equals("DELETE")) {
                    arr.remove(i);
                    clearFile = arr.size() == 0;
                }
            }

            String outputLine = !clearFile ? String.join("\n", arr) + '\n' : "";
            Files.writeString(path, outputLine);
        } catch (IOException e) {
            System.out.println(e.toString());
            return false;
        }

        return true;
    }

    @Override
    public Map<String, Integer> getTaskValues(String username, int ID) {
        List<String> lines = getTasks(username);
        Map<String, Integer> values = new HashMap<>();

        for (String line : lines) {
            int fileID = Integer.parseInt(line.split(" ")[0]);

            if (fileID == ID) {
                String[] lineParams = line.split(" ");
                values.put("value1", Integer.parseInt(lineParams[1]));
                values.put("value2", Integer.parseInt(lineParams[2]));
            }
        }

        return values;
    }

    @Override
    public int getLastID(String username) {
        List<String> lines = getTasks(username);
        boolean nullSize = lines.size() == 0;
        return !nullSize ? Integer.parseInt(lines.get(lines.size() - 1).split(" ")[0]) + 1 : 1;
    }
}
