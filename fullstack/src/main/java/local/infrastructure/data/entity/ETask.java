package local.infrastructure.data.entity;

import java.io.Serializable;

import jakarta.persistence.*;

@Entity
@Table(name = "\"TASKS\"")
public class ETask implements Serializable {
    @Id
    @Column(name = "id")
    private Integer ID;

    @Column(name = "value1")
    private Integer value1;

    @Column(name = "value2")
    private Integer value2;

    @Column(name = "result")
    private String result;

    @Column(name = "status")
    private String status;

    @Column(name = "user_id")
    private Integer userID;

    public Integer getID() {
        return ID;
    }

    public Integer getValue1() {
        return value1;
    }

    public Integer getValue2() {
        return value2;
    }

    public String getResult() {
        return result;
    }

    public String getStatus() {
        return status;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public void setValue1(Integer value1) {
        this.value1 = value1;
    }

    public void setValue2(Integer value2) {
        this.value2 = value2;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }
}
