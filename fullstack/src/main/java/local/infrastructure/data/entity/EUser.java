package local.infrastructure.data.entity;

import java.io.Serializable;

import jakarta.persistence.*;

@Entity
@Table(name = "\"USERS\"")
public class EUser implements Serializable {
    @Id
    @Column(name = "id")
    private Integer ID;

    @Column(name = "name")
    private String name;

    @Column(name = "password")
    private String password;

    public Integer getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public void setID(Integer userID) {
        ID = userID;
    }

    public void setName(String userName) {
        name = userName;
    }

    public void setPassword(String userPassword) {
        password = userPassword;
    }
}
