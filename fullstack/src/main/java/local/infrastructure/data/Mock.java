package local.infrastructure.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Mock implements IFileExplorer {
    public static String[] users = new String[]{"denio"};
    public static String[] passwds = new String[]{"test"};

    public static Map<String, List<String>> tasks = new HashMap<>();

    @Override
    public boolean findUser(String username, String passwd) {
        boolean userFound = false;
        boolean passwdFound = false;

        for (String user : users) {
            if (user.equals(username)) {
                userFound = true;
                break;
            }
        }

        for (String pass : passwds) {
            if (pass.equals(passwd)) {
                passwdFound = true;
                break;
            }
        }

        return userFound && passwdFound;
    }

    @Override
    public List<String> getTasks(String username) {
        List<String> lines = tasks.get(username);

        return lines != null ? lines : new ArrayList<>();
    }

    @Override
    public boolean createTask(String username, int value1, int value2) {
        String IDStr = String.valueOf(getLastID(username));
        String value1Str = String.valueOf(value1);
        String value2Str = String.valueOf(value2);
        String[] taskParams = new String[]{IDStr, value1Str, value2Str, "X", "Waiting"};

        if (tasks.get(username) == null) {
            tasks.put(username, new ArrayList<>());
        }

        tasks.get(username).add(String.join(" ", taskParams));

        return true;
    }

    @Override
    public boolean modifyTask(String username, int ID, int result, String status) {
        List<String> arr = getTasks(username);

        for (int i = 0; i < arr.size(); i++) {
            int fileID = Integer.parseInt(arr.get(i).split(" ")[0]);

            if (fileID == ID && status.equals("Processing")) {
                String new_value = arr.get(i).replace("Waiting", status);
                arr.set(i, new_value);
            } else if (fileID == ID && status.equals("Processed")) {
                String new_value = arr.get(i).replace("X", String.valueOf(result)).replace("Processing", status);
                arr.set(i, new_value);
            } else if (fileID == ID && status.equals("DELETE")) {
                arr.remove(i);
            }
        }

        return true;
    }

    @Override
    public Map<String, Integer> getTaskValues(String username, int ID) {
        List<String> lines = getTasks(username);
        Map<String, Integer> values = new HashMap<>();

        for (String line : lines) {
            int fileID = Integer.parseInt(line.split(" ")[0]);

            if (fileID == ID) {
                String[] lineParams = line.split(" ");
                values.put("value1", Integer.parseInt(lineParams[1]));
                values.put("value2", Integer.parseInt(lineParams[2]));
            }
        }

        return values;
    }

    @Override
    public int getLastID(String username) {
        List<String> lines = getTasks(username);
        boolean nullSize = lines.size() == 0;
        return !nullSize ? Integer.parseInt(lines.get(lines.size() - 1).split(" ")[0]) + 1 : 1;
    }
}

