package local.infrastructure.data;

import jakarta.annotation.Resource;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;
import jakarta.transaction.UserTransaction;
import local.infrastructure.data.entity.ETask;
import local.infrastructure.data.entity.EUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;

public class DBExplorer implements IFileExplorer {

    private EntityManager entityManager;

    @Resource
    private UserTransaction userTransaction;

    DBExplorer() {
        try {
            InitialContext context = new InitialContext();
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("fullstack_pu");
            entityManager = emf.createEntityManager();
            userTransaction = (UserTransaction) context.lookup("java:comp/UserTransaction");
        }
        catch (NamingException e) {
            e.printStackTrace();
            System.out.println("NamingException in catch DBExplorer");
        }
    }

    @Override
    public boolean findUser(String username, String passwd) {
        boolean isNotNull = false;

        try {
            String jpql = "SELECT u FROM EUser u WHERE u.name = :name AND u.password = :password";
            Query query = entityManager.createQuery(jpql);
            query.setParameter("name", username);
            query.setParameter("password", passwd);

            List<EUser> users = query.getResultList();

            isNotNull = users != null && !users.isEmpty();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception in findUser");
        }

        return isNotNull;
    }

    @Override
    public List<String> getTasks(String username) {
        List<String> lines = new ArrayList<>();

        try {
            String jpql = "SELECT t FROM ETask t WHERE t.userID = (SELECT u.ID FROM EUser u WHERE u.name = :name)";
            Query query = entityManager.createQuery(jpql);
            query.setParameter("name", username);

            List<ETask> tasks = query.getResultList();
            if (tasks == null || tasks.isEmpty()) return lines;

            for (ETask task : tasks) {
                String[] params = new String[]{String.valueOf(task.getID()), String.valueOf(task.getValue1()),
                        String.valueOf(task.getValue2()), task.getResult(), task.getStatus(),
                        String.valueOf(task.getUserID())};
                String line = String.join(" ", params);
                lines.add(line);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception in getTasks");
        }

        return lines;
    }

    @Override
    @Transactional
    public boolean createTask(String username, int value1, int value2) {
        try {
            userTransaction.begin();
            entityManager.joinTransaction();

            String jpql = "SELECT u from EUser u WHERE u.name = :name";
            Query query = entityManager.createQuery(jpql);
            query.setParameter("name", username);
            List<EUser> users = query.getResultList();
            if (users == null || users.isEmpty()) return false;

            ETask task = new ETask();
            task.setID(getLastID() + 1);
            task.setValue1(value1);
            task.setValue2(value2);
            task.setResult("X");
            task.setStatus("Waiting");
            task.setUserID(users.get(0).getID());
            entityManager.persist(task);

            userTransaction.commit();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception in createTask");
        }

        return true;
    }

    @Override
    @Transactional
    public boolean modifyTask(String username, int ID, int result, String status) {
        try {
            userTransaction.begin();
            entityManager.joinTransaction();

            ETask editableTask = entityManager.find(ETask.class, ID);
            if (editableTask == null) return false;
            switch (status) {
                case "Processing" -> {
                    editableTask.setStatus(status);
                    entityManager.merge(editableTask);
                }
                case "Processed" -> {
                    editableTask.setStatus(status);
                    editableTask.setResult(String.valueOf(result));
                    entityManager.merge(editableTask);
                }
                case "DELETE" -> entityManager.remove(editableTask);
            }

            userTransaction.commit();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception in modifyTask");
        }

        return true;
    }

    @Override
    public Map<String, Integer> getTaskValues(String username, int ID) {
        Map<String, Integer> values = new HashMap<>();

        try {
            ETask task = entityManager.find(ETask.class, ID);
            if (task != null) {
                values.put("value1", task.getValue1());
                values.put("value2", task.getValue2());
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception in getTaskValues");
        }

        return values;
    }

    @Override
    public int getLastID(String username) {
        List<String> lines = getTasks(username);
        boolean nullSize = lines.size() == 0;
        return !nullSize ? Integer.parseInt(lines.get(lines.size() - 1).split(" ")[0]) + 1 : 1;
    }

    private int getLastID() {
        int id = 0;

        try {
            String jpql = "SELECT t FROM ETask t ORDER BY t.ID DESC";
            Query query = entityManager.createQuery(jpql);
            List<ETask> tasks = query.getResultList();
            if (tasks != null && !tasks.isEmpty()) id = tasks.get(0).getID();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception in getLastID");
        }

        return id;
    }
}