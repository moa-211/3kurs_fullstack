import Router from './page/router.js';

(async () => {
    const router = new Router();
    
    router.addPage('login' , 'login-page');
    router.addPage('tasks', 'task-page');
    router.setDefault('login-page');
    
    await router.showPage('test');
})();
