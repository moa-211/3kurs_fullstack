const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
    mode: 'production',	
    entry: './index.js',
    output: {
      path: path.resolve(__dirname, './dist'),
      filename: 'bundle.js'
    },
    module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.m?ts$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'ts-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
   },
   performance: {
    hints: false
   },
   devServer: {
    static: {
        directory: path.resolve(__dirname, './dist')
    },
    port: 9090
   },
   plugins: [new HtmlWebpackPlugin()]
  };