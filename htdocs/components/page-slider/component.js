import template from './template.js'

// Importing manager and router
import Manager from '../../state-management/manager.js'
import Router from '../../page/router.js'

class PageSlider extends HTMLElement {
    constructor() { 
        super();
        this.shadow = this.attachShadow({mode: 'closed'});

        // Saving manager and router (Singletone)
        this.manager = new Manager();
        this.router = new Router();

        this.username = this.manager.getState('username');
    }

    connectedCallback() {
        // Rendering
        this.render();
    }

    disconnectedCallback() {}

    static get observedAttributes() {
        return [];
    }

    attributeChangedCallback(attr, oldValue, newValue) {}

    render() {
        // Filling in shadow root by template
        this.shadow.innerHTML = template(this);

        // Initializing events
        this.initEvents();
    }

    initEvents() {
        this.shadow.querySelector('input').addEventListener('click', () => {
            this.manager.updateState('username', '');
            this.manager.updateState('passwd', '');
            this.router.showPage('login');
        });
    }
}

customElements.define('page-slider', PageSlider);
