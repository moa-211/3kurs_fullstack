// Importing components
import template from './template.js'

// Importing manager and router
import Manager from '../../state-management/manager.js'
import Router from '../../page/router.js'

// Importing util function to show error
import {showError} from '../../utils.js'

class LoginWindow extends HTMLElement {
    constructor() { 
        super();
        this.shadow = this.attachShadow({mode: 'closed'});
        this.subscriptions = [];

        this.initFields(); // Initializing fields to save local state

        // Saving manager and router (Singletone)
        this.manager = new Manager();
        this.router = new Router();
    }

    initFields() {
        this.username = '';
        this.passwd = '';
    }

    async connectedCallback() {
        // Subscribe to manager's states change
        this.subscriptions.push(await this.manager.subscribe('login', this.checkState.bind(this)));

        // Rendering
        this.render();
    }

    disconnectedCallback() {
        this.initFields();
        this.unsubscribe();
    }

    static get observedAttributes() {
        return [];
    }

    attributeChangedCallback(attr, prev, next) {}

    render() {
        // Filling in shadow root by template
        this.shadow.innerHTML = template(this);

        // Initializing events
        this.initEvents();
    }

    initEvents() {
        // Listeners to have actual information about attributes in class fields
        let usernameField = this.shadow.querySelector('#username');
        usernameField.addEventListener('change', event => {
            this.username = event.target.value;
            this.manager.updateState('username', this.username);
        });
        
        let passwdField = this.shadow.querySelector('#passwd');
        passwdField.addEventListener('change', event => {
            this.passwd = event.target.value;
            this.manager.updateState('passwd', this.passwd);
        });

        // State management logic
        let loginBtn = this.shadow.querySelector('#login-btn');
        loginBtn.addEventListener('click', () => {
            if (!this.username) {
                showError(usernameField, 'Введите логин!');
                return;
            }
            if (!this.passwd) {
                showError(passwdField, 'Введите пароль!');
                return;
            }
            this.manager.query('login');
        });
    }

    checkState(stateName, state) {
        switch(stateName) {
            case 'login':
                if (state.status === 'OK') {
                    this.username = state.username;
                    this.router.showPage('tasks');
                }
                else showError(this.shadow.querySelector('#passwd'), 'Неправильный логин и/или пароль!');
                break;
        }
    }

    unsubscribe() {
        this.subscriptions.forEach(callback => {
            this.manager.unsubscribe(callback);
        });
    }
}

customElements.define('login-window', LoginWindow);
