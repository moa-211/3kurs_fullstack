import './login-page/component.js'
import './task-page/component.js'

export default class Router {
    static instance = null;

    constructor() {
        this.routes = {};

        if (!Router.instance) Router.instance = this;
        else return Router.instance;
    }

    addPage(url, pageName) {
        this.routes[url] = document.createElement(pageName);
    }

    setDefault(pageName) {
        for (let page of Object.values(this.routes))
            if (pageName === page.tagName.toLowerCase()) {
                this.routes.default = page;
                return;
            }

        this.routes.default = document.createElement(pageName);
    }

    async showPage(url='default') {
        if (!Object.keys(this.routes).includes(url)) url = 'default';

        document.body.innerHTML = '';
        document.body.appendChild(this.routes[url]);
    }
}
